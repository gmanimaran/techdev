﻿using Media.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Test.TestClasses
{
    public class MediaTestDataAccess : IMediaDataAccess
    {
        public List<Media.Entity.Media> GetMediaList()
        {
            List<Media.Entity.Media> mediaList = new List<Media.Entity.Media>();
            mediaList.Add(new Media.Entity.Media() { TitleId = 1, ReleaseYear = 2004, TitleName = "Titanic" });
            mediaList.Add(new Media.Entity.Media() { TitleId = 2, ReleaseYear = 2009, TitleName = "Avatar" });
            return mediaList;
        }

        public List<Media.Entity.Media> GetMediaList(string Title)
        {
            throw new NotImplementedException();
        }
        public Media.Entity.Media GetMediaDetails(int TitleId)
        {
            throw new NotImplementedException();
        }

        public List<Media.Entity.Cast> GetCastList(int TitleId)
        {
            throw new NotImplementedException();
        }

        public List<Media.Entity.Genre> GetGenreList(int TitleId)
        {
            throw new NotImplementedException();
        }

        public List<Media.Entity.Award> GetAwardList(int TitleId)
        {
            throw new NotImplementedException();
        }

        public List<Media.Entity.StoryLine> GetStoryLineList(int TitleId)
        {
            throw new NotImplementedException();
        }
        

               
    }
}
