﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Media.Web.Models;
using Movie.Web.MediaServiceReference;


namespace Media.Web.Controllers
{
    public class TitlesController : Controller
    {
        // Invoking Media Service Instances.
        public MediaServiceClient mediaService = new MediaServiceClient();
        // Loading all the Titles.
        public ActionResult Index()
        {
            return View(mediaService.Default());
        }
        //Filtering Title List by Title
        [HttpPost]
        public ActionResult Index(string TitleSearch)
        {
            var Title = mediaService.GetMediaList(TitleSearch).ToList();

            return View(Title);
        }
        // Getting Title Details for single Title
        public ActionResult Details(int id = 0)
        {
            string GenreName = "";

            var Title = mediaService.GetMediaDetails(id);
            if (Title == null)
            {
                return HttpNotFound();
            }
            else
            {
                foreach (var n in mediaService.GetGenreList(id).ToList())
                {
                    GenreName = GenreName + n.Name + " | ";
                }

                ViewBag.GenreName = GenreName.Remove(GenreName.LastIndexOf("|"));

            }
            return View(Title);
        }
        //// Getting Award Details for Title
        public ActionResult AwardDetails(int TitleId = 0)
        {
            var Award = mediaService.GetAwardList(TitleId).ToList();
            if (Award == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.TitleName = mediaService.GetMediaDetails(TitleId).TitleName;
            }

            return View(Award);
        }
        //// Getting Cast Details for Title
        public ActionResult Cast(int TitleId = 0)
        {
            var Cast = mediaService.GetCastList(TitleId).ToList();
            if (Cast == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.TitleName = mediaService.GetMediaDetails(TitleId).TitleName;
            }
            return View(Cast);
        }
        // //Getting StoryLines Details for Title
        public ActionResult StoryLines(int TitleId = 0)
        {

            var StoryLines = mediaService.GetStoryLineList(TitleId).ToList();
            if (StoryLines == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.TitleName = mediaService.GetMediaDetails(TitleId).TitleName;
                return View(StoryLines);
            }

        }
    }
}