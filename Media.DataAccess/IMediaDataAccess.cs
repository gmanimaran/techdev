﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media.DataAccess
{   
    public interface IMediaDataAccess
    {
        List<Media.Entity.Media> GetMediaList();
        List<Media.Entity.Media> GetMediaList(string Title);
        Media.Entity.Media GetMediaDetails(int TitleId);
        List<Media.Entity.Cast> GetCastList(int TitleId);
        List<Media.Entity.Genre> GetGenreList(int TitleId);   
        List<Media.Entity.Award> GetAwardList(int TitleId);
        List<Media.Entity.StoryLine> GetStoryLineList(int TitleId);   
    }
}
