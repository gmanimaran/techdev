﻿using Media.DataAccess;
using Media.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Media.Business
{
    public class MediaBusiness
    {   //Implemented the Factory Design Pattern .
        //Based on the Media Type will load the data in the Object.
        IMediaDataAccess mediaDA;

        public MediaBusiness()
        {
            mediaDA = MediaFactory.GetMediaFactory();
        }

        public MediaBusiness(IMediaDataAccess da, bool isTest)
        {
            mediaDA = da;
        }

        public List<Media.Entity.Media> GetMediaList(string Title)
        {
            return mediaDA.GetMediaList(Title);

        }
        public List<Media.Entity.Media> GetMediaList()
        {
            return mediaDA.GetMediaList();

        }
        public Media.Entity.Media GetMediaDetails(int TitleId)
        {
            return mediaDA.GetMediaDetails(TitleId);
        }
        
        public List<Cast> GetCastList(int TitleId)
        {
            return mediaDA.GetCastList(TitleId);
        }

        public List<Genre> GetGenreList(int TitleId)
        {
            return mediaDA.GetGenreList(TitleId);
        }

        public List<Award> GetAwardList(int TitleId)
        {
            return mediaDA.GetAwardList(TitleId);
        }

        public List<StoryLine> GetStoryLineList(int TitleId)
        {
            return mediaDA.GetStoryLineList(TitleId);
        }
    }
}