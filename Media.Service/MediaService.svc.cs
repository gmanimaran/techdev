﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Media.Business;
using Media.Entity;

namespace Media.Service
{
     public class MediaService : IMediaService
    {
        public MediaBusiness movieBL = null;

        public List<Entity.Media> GetMediaList(string Title)
        {
            movieBL = new MediaBusiness();

            return movieBL.GetMediaList(Title);
        }
        public List<Entity.Media> GetMediaList()
        {
            movieBL = new MediaBusiness();

            return movieBL.GetMediaList();
        }

        public Entity.Media GetMediaDetails(int TitleId)
        {
            movieBL = new MediaBusiness();

            return movieBL.GetMediaDetails(TitleId);
        }

        public List<Entity.Cast> GetCastList(int TitleId)
        {
            movieBL = new MediaBusiness();

            return movieBL.GetCastList(TitleId);
        }

        public List<Entity.Genre> GetGenreList(int TitleId)
        {
            movieBL = new MediaBusiness();

            return movieBL.GetGenreList(TitleId);
        }

        public List<Entity.Award> GetAwardList(int TitleId)
        {
            movieBL = new MediaBusiness();

            return movieBL.GetAwardList(TitleId);
        }

        public List<Entity.StoryLine> GetStoryLineList(int TitleId)
        {
            movieBL = new MediaBusiness();

            return movieBL.GetStoryLineList(TitleId);
        }
    }
}
