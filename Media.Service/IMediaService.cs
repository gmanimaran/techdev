﻿using Media.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Media.Service
{
    [ServiceContract]
    public interface IMediaService
    {
        [OperationContract]
        List<Media.Entity.Media> GetMediaList(string Title);
        [OperationContract(Name ="Default")]
        List<Media.Entity.Media> GetMediaList();
        [OperationContract]
        Media.Entity.Media GetMediaDetails(int TitleId);
        [OperationContract]
        List<Cast> GetCastList(int TitleId);
        [OperationContract]
        List<Genre> GetGenreList(int TitleId);
        [OperationContract]
        List<Award> GetAwardList(int TitleId);
        [OperationContract]
        List<StoryLine> GetStoryLineList(int TitleId);       
    }
       
}
